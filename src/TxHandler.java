import java.security.PublicKey;
import java.util.*;
import java.util.stream.Collectors;

public class TxHandler {

    /**
     * Creates a public ledger whose current UTXOPool (collection of unspent transaction outputs) is
     * {@code utxoPool}. This should make a copy of utxoPool by using the UTXOPool(UTXOPool uPool)
     * constructor.
     */
    private final UTXOPool unspendTxPool;

    public TxHandler(UTXOPool utxoPool) {
        this.unspendTxPool = utxoPool;
    }

    /**
     * @return true if:
     * (1) all outputs claimed by {@code tx} are in the current UTXO pool, DONE
     * (2) the signatures on each input of {@code tx} are valid, DONE
     * (3) no UTXO is claimed multiple times by {@code tx},
     * (4) all of {@code tx}s output values are non-negative, and DONE
     * (5) the sum of {@code tx}s input values is greater than or equal to the sum of its output DONE
     * values; and false otherwise.
     */
    public boolean isValidTx(Transaction tx) {
        return !hasNegativeOutputs(tx)
                && !hasDoubleSpend(tx)
                && !outputValueExceedsInputValue(tx)
                && !hasInvalidSignature(tx);
    }

    /**
     * Handles each epoch by receiving an unordered array of proposed transactions, checking each
     * transaction for correctness, returning a mutually valid array of accepted transactions, and
     * updating the current UTXO pool as appropriate.
     */
    public Transaction[] handleTxs(Transaction[] possibleTxs) {
        List<Transaction> validTxs = Arrays.stream(possibleTxs)
                .filter(this::isValidTx)
                .map(transaction -> {
                    updateUnspentTxPoolWithInputs(transaction);
                    updateUnspentTxPoolWithOutputs(transaction);
                    return transaction;
                })
                .collect(Collectors.toList());

        return validTxs.toArray(new Transaction[validTxs.size()]);
    }

    private void updateUnspentTxPoolWithInputs(Transaction tx) {
        for(int i=0 ; i<tx.getInputs().size(); i++) {
            Transaction.Input input = tx.getInput(i);
            unspendTxPool.removeUTXO(new UTXO(input.prevTxHash, input.outputIndex));
        }
    }

    private void updateUnspentTxPoolWithOutputs(Transaction tx) {
        for(int i=0 ; i<tx.getOutputs().size() ; i++) {
            Transaction.Output output = tx.getOutput(i);
            unspendTxPool.addUTXO(new UTXO(tx.getHash(), i), output);
        }
    }

    /**
     * @return 'true' if any of the outputs in a{@link Transaction} has
     * a negative value;
     */
    private boolean hasNegativeOutputs(Transaction tx) {
        return tx.getOutputs().stream()
                .anyMatch(output -> output.value < 0);
    }

    /**
     * @return 'true' if the {@link Transaction} consumes the same input more than once, or if an input
     * is not in the provided {@link UTXOPool}.
     */
    private boolean hasDoubleSpend(Transaction tx) {
        Set<UTXO> claimedUTXO = new HashSet<>();
        for (Transaction.Input input : tx.getInputs()) {
            UTXO sourceUTXO = new UTXO(input.prevTxHash, input.outputIndex);

            // to handle double spend
            if (!unspendTxPool.contains(sourceUTXO) || claimedUTXO.contains(sourceUTXO)) {
                return true;
            } else {
                claimedUTXO.add(sourceUTXO);
            }
        }
        return false;
    }

    /**
     * @return 'true' if the {@link Transaction}'s outputs has more value that the inputs.
     */
    private boolean outputValueExceedsInputValue(Transaction tx) {
        double inputSum = 0.0;
        for (Transaction.Input input : tx.getInputs()) {
            if (getValueForInput(input) == -1) {
                return false;
            } else {
                inputSum += getValueForInput(input);
            }
        }

        double outputSum = tx.getOutputs().stream()
                .mapToDouble(this::getValueForOutput)
                .sum();

        return inputSum < outputSum;
    }

    private double getValueForInput(Transaction.Input input) {
        UTXO sourceUTXO = new UTXO(input.prevTxHash, input.outputIndex);
        return unspendTxPool.contains(sourceUTXO)
                ? unspendTxPool.getTxOutput(sourceUTXO).value
                : -1;
    }

    private double getValueForOutput(Transaction.Output output) {
        return output.value;
    }

    private boolean hasInvalidSignature(Transaction tx) {
        for(int i=0 ; i<tx.getInputs().size() ; i++) {
            Transaction.Input input = tx.getInput(i);
            Optional<PublicKey> ownerAddress = getOwnerAddress(input);
            if(!(ownerAddress.isPresent() && Crypto.verifySignature(
                    ownerAddress.get(),
                    tx.getRawDataToSign(i),
                    input.signature
                )
            )) {
                return true;
            }
        }
        return false;
    }

    private Optional<PublicKey> getOwnerAddress(Transaction.Input input) {
        UTXO sourceUTXO = new UTXO(input.prevTxHash, input.outputIndex);
        if(unspendTxPool.contains(sourceUTXO)) {
            Transaction.Output sourceOutput = unspendTxPool.getTxOutput(sourceUTXO);
            return Optional.of(sourceOutput.address);
        } else {
            return Optional.empty();
        }
    }
}
